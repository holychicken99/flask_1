from flask import Flask, render_template, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask import redirect,request

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

# @app.route('/')
# def hello():
#     return render_template('fallhack20.html'

# sno , name , date , content
# class Elements(db.Model):

#     sno = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(80),  nullable=False)
#     date = db.Column(db.String(120),  nullable=False)
#     content = db.Column(db.String(120),  nullable=False)


# @app.route("/contact", methods = ['GET', 'POST'])


# class TODO(db.Model):

#     id=db.Column(db.Integer,primary_key=True)
#     content=db.Column(db.String(200),nullable=False)
#     date_created=db.Column(db.datetime,default=datetime.utcnow)

#     def __rept__(self):
#         return '<Task %r' %self.id


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/hello/<n>')
def none(n):
    s = " you fkav entered   string  %s " % n
    return s



@app.route('/user/<string:name>')
def user_auth(name):
    if(name == 'admin'):

        return redirect(url_for('home'))
    else:
        return redirect(url_for('none',n='works'))

#url_for('takes an arguement as the function name and anothen as the passing arguements)
        # we use redirect as the name suggest to redirect the the webpages to each other instead of repeatedly defining the function


# adding a #makes the format of formatable string usable



@app.route('/form/<string:name>')
def form_man(name):
   return name

if __name__ == '__main__':
    app.run(debug=True)
