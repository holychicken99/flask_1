from flask import Flask, render_template, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask import redirect,request,abort
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import SubmitField
import os
from werkzeug.utils import secure_filename

#here we have used an extension to handle the file uploading system






app=Flask(__name__)


app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.gif']
app.config['UPLOAD_PATH'] = 'uploads'

class MyForm(FlaskForm):
    file = FileField('File')
    submit = SubmitField('Submit')

if __name__ == '__main__':  
    app.run(debug=True)





@app.route('/')
def index():
    return render_template('index.html')

@app.route('/', methods=['POST'])
def upload_files():
    uploaded_file = request.files['file']
    filename = secure_filename(uploaded_file.filename)
    if filename != '':
        file_ext = os.path.splitext(filename)[1]#here  the extension of the file is checked 
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            abort(400)
        uploaded_file.save(os.path.join(app.config['UPLOAD_PATH'], filename))#        uploaded_file.save() takes 2 arguements the path where the file is to be saved anf its name
    return redirect(url_for('index'))